GPU_ID=$1
INTPUT=$2
OUTPUT=$3 

echo "[INFO] Pull docker image from Docker Hub"
docker pull 20011911/notsecond_vaipe_challenge:latest
echo "[INFO] Build container"
docker build -t vaipe_notsecond:latest .
echo "[INFO] Start running"
docker run -it --rm --gpus=$GPU_ID --shm-size=8G -v $INTPUT:/pill/RELEASE_private_test -v $OUTPUT:/pill/records vaipe_notsecond:latest