# [Solution] AI4VN 2022 - VAIPE: Medicine Pill Image Recognition Challenge

```
Solution of NotSecond@AIClubteam at Private Test phase.
```
**Update:**
1. Các checkpoints trọng số của mô hình pill/pres detection và pill recogniztion không thay đổi.
2. Thay đổi pre-train mô hình VietOCR từ seq2seq sang transformer.
3. Sử dụng pre-train CLIP để embedding các tập ảnh random từ 106 label và sử dụng Kmeans để clustering => Xác định các label dễ bị nhầm lẫn nhau. 
4. Thay đổi thuật toán module process pill recognize.

**Note:**
1. Thực hiện lại các bước mô tả bên dưới để reproduce kết quả bao gồm bước cập nhập Docker Image từ Docker Hub.
2. Chỉ truyền một GPU_ID vào container.
3. Pipeline chỉ xử lý tuần tự nên thời gian inference khá lâu trên toàn bộ private test (dự kiến 3h).

<span id='ff'></span>
##  Folder Format
<span id='data'></span>
### 1. Datasets
```
<vaipe_notsecond>

|-- RELEASE_private_test/
    |-- pill/
    |   `-- image/
    |-- prescription/
    |    `-- image/
    `-- pill_pres_map.json 
|-- random_on_class
```

<span id='source'></span>
2. Source and scripts
```
<vaipe_notsecond>
|-- cfg/
|   |-- cfg_pill_detect_vaipe.cfg
|   |-- cfg_pill_recognize_vaipe.yaml
|   |-- mapped_presname2classid.json
|   `-- setting.py
|
|-- src/
|   |-- detect/
|   |    |-- yolor/
|   |    `-- yolov7/
|   `-- classify/
|-- tools/
|   |-- common.py
|   |-- pill_detect.py
|   |-- pill_recognize.py
|   |-- pill_recognize_resnet18.py
|   |-- pres_detect.py
|   |-- kmeans_cluster.py
|   |-- img_embed.py
|   |-- pres_recognize.py
|   |-- process.py
|   `-- main.py
|-- weights/
|   |-- pill_detection_checkpoint.pt
|   |-- pill_recognition_checkpoint.pt
|   |-- pill_recognize_resne18.pt
|   |-- pres_detection_checkpoint.pt
|   `-- ViT-L-14-336px.pt
|-- records/
|-- Dockerfile
`-- run.sh
```

<span id="setup"><span>
## Setup and inference
<span id='sbs'></span>
### 1. Step by step

```bash
# 1. Clone repository
git clone https://gitlab.com/namnh-dev/vaipe_notsecond.git
cd vaipe_notsecond

# 2.Build Docker Image
# 2.1. Pull docker image from DockerHub
docker pull 20011911/notsecond_vaipe_challenge:latest

# 2.2. Re-build docker image with load weights pretrain
docker build -t vaipe_notsecond:latest Dockerfile

# 3. Start Inference
docker run -it --rm \           
        --gpus=<gpu_id> \                               # Choose gpu_ids
        --shm-size=8G \                                 # Share memory from local
        -v </path/RELEASE_privatest>:/pill/public_test \  # Datas input path
        -v </path/output>:/pill/records \               # Outputs path
        vaipe_notsecond:latest                          # Docker Image Nam

#Example:
docker run -it --rm --gpus="0" --shm-size=8G -v /path/dataset/RELEASE_privatest:/pill/public_test -v /path/resubmit/output:/pill/records vaipe_notsecond:latest

# 4. Check output
# After running, results.csv and log.log output file with save in /path/resubmit/output or vaipe_notsecond/records/
```

<span id="aio"><span>
## All in one
```bash

# Clone repository
git clone https://gitlab.com/namnh-dev/vaipe_notsecond.git
cd vaipe_notsecond

# Run all step with only file
bash setup_and_run.sh <gpu_id> </path/public_test> </path/output>

#Example:
bash setup_and_run.sh 0 /path/dataset/public_test_new /path/resubmit/output
#After running, results.csv and log.log output file with save in /path/resubmit/output or vaipe_notsecond/records/
```

Log file when successful

<img src="imgs/log.png" width="425"/>

## Output structure
```bash
|records/
    |-- visualize/      #Visualize output
    |   |-- pill/
    |   `-- pres/
    |-- log.log         # Log file
    |-- results.csv     # Final result using for submission .
    `-- ...
```
<span id="vis"></span>
## Visualize
<img src="imgs/VAIPE_P_TEST_NEW_103.png" width="425"/> <img src="imgs/VAIPE_P_103_0.jpg" width="425"/>
