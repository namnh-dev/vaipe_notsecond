# GPU_ID using inference.

GPU_ID = 0
# Visualize
VISUALIZE = True

#====================================================================================
# Inputs path
IMAGE_PILL_DIR = 'RELEASE_private_test/pill/image'
IMAGE_PRES_DIR = 'RELEASE_private_test/prescription/image'
PRES_MAP_FILE =  'RELEASE_private_test/pill_pres_map.json'

IMG_RANDOMCLASS_DIR = "random_on_class/images"
#====================================================================================
# Weights path
WEIGHT_DETECT_PRES = 'weights/pres_detection_checkpoint.pt'
WEIGHT_DETECT_PILL = 'weights/pill_detection_checkpoint.pt'
WEIGHT_RECOGNIZE_PILL = 'weights/pill_recognition_checkpoint.pt'
WEIGHT_RECOGNIZE_PILL_RESNET18 = 'weights/pill_recognize_resne18.pt'

#====================================================================================
# Config file
MAP_NAME_TO_CLASS_PILL = 'cfg/mapped_presname2classid.json'
CFG_DET_PILL = 'cfg/cfg_pill_detect_vaipe.cfg'
CFG_CLS_PILL = 'cfg/cfg_pill_recognize_vaipe.yaml'


#====================================================================================
# Outpath
PRES_CSV_OUTFILE = 'records/pres_detection_outfile.csv'
MAP_PRES2PILL_OUTFILE = 'records/mapped_pres2pill_outfile.json'
PILL_DET_OUTFILE = 'records/pill_detection_outfile.csv'
PILL_REG_OUTFILE = 'records/pill_recognize_outfile.csv'
PILL_REG_RESNET_OUTFILE = 'records/pill_recognize_resnet_outfile.csv'
OUT_FINAL = 'records/results.csv'

IMG_CLS_EMBED = 'records/img_embed.json'
PILL_CLUSTER = 'records/pill_cluster.json'

# Save visualize
SAVE_VISUAL_PRES = 'records/visualize/pres'
SAVE_VISUAL_PILL = 'records/visualize/pill'

#====================================================================================
# Python path
PRES_DETECT_PATH = 'src/detect/yolov7'
DETECT_PATH = 'src/detect/yolor'
RECOGNIZE_PATH = 'src/classify'


#====================================================================================
# Config parameter
IMGSIZE_PRES = 1280
PRES_CONF_THRES = 0.25
PRES_IOU_THRES = 0.45
PILL_CONF_THRES = 0.3
PILL_IOU_THRES = 0.5
IMGSIZE_PILL = 1280
IMGSIZE_CLS = 384

