'''
Last update: 21/07/2022
'''
import json
import os

from tqdm import tqdm
import cv2

folder_pill = "/pill/public_train/pill"
folder_output = "/pill/src/classify/datasets"


def _only_file(fn: str):
    image_p = os.path.join(folder_pill, "image", fn)
    label_p = os.path.join(folder_pill, "label", f"{fn.split('.')[0]}.json")
    image = cv2.imread(image_p)
    annotations = json.load(open(label_p, 'r'))

    for cnt, annot in enumerate(annotations):
        x, y, w, h, label = tuple(annot[c] for c in ["x", "y", "w", "h", "label"])
        cropped_image = image[y:y+h, x:x+w]

        save_fn = f"{fn.split('.')[0]}_{str(label)}_{cnt}.jpg"
        
        if not os.path.exists(os.path.join(folder_output, str(label))):
            os.makedirs(os.path.join(folder_output, str(label)))
        
        cv2.imwrite(os.path.join(folder_output, str(label), save_fn), cropped_image)
        
    pass


def main():
    for fn in tqdm(os.listdir(os.path.join(folder_pill, 'image'))):
        _only_file(fn)
 

if __name__=="__main__":
    main()


