import math
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import torchvision.transforms as T
import torchvision.models as models
import matplotlib.pyplot as plt

from torch.utils.data import Dataset, DataLoader, random_split
from PIL import Image, ImageDraw, ImageFont
from pathlib import Path
from tqdm import tqdm

from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix, classification_report

from loss import FocalLoss

from sklearn.utils.class_weight import compute_class_weight

import random

import albumentations as A
from albumentations.pytorch import ToTensorV2

from optimizers import SAM

device = "cuda" if torch.cuda.is_available() else "cpu"


np.set_printoptions(suppress=True)
torch.set_printoptions(precision=8, sci_mode=False)


def compute_weights_for_wloss(label_file):
    y = []
    with label_file.open("r", encoding="utf-8") as f:
        for l in f:
            p, label = l.strip().split("\t")
            label = int(label)
            if label == 107:
                continue

            y.append(label)

    return compute_class_weight(
        class_weight="balanced",
        classes=list(set(y)),
        y=y,
    )


class PillDataset(Dataset):
    def __init__(self, root, label_file, transform=None, mode="train"):
        if isinstance(root, str):
            root = Path(root)

        self.root = root
        self.transform = transform
        self.mode = mode

        label_file = root / label_file
        images = []
        labels = []
        class_map = {}

        with label_file.open("r", encoding="utf-8") as f:
            for l in f:
                p, label = l.strip().split("\t")
                label = int(label)
                if label == 107:
                    continue

                images.append(p)
                labels.append(label)

                if label not in class_map:
                    class_map[label] = []
                class_map[label].append((p, label))

        self.class_map = class_map
        self.images = images
        self.labels = labels
        self.n_classes = len(set(labels))

    def __len__(self):
        # if self.mode == "train":
        #     return int(len(self.images) * 1.3)
        return len(self.images)

    def __getitem__(self, idx):
        # if self.mode == "train":
        #     clsId = random.choices(
        #         population=list(range(107)),
        #         weights=[1] * 107,
        #         k=1,
        #     )[0]
        #     path, label = random.choice(self.class_map[clsId])
        # else:
        #     path, label = self.images[idx], self.labels[idx]

        # orin_img = Image.open(str(self.root / path))
        # orin_img = orin_img.resize((224, 224))
        # if self.transform:
        #     orin_img = np.array(orin_img)
        #     img = self.transform(image=orin_img)["image"]

        # return img, label
        orin_img = Image.open(str(self.root / self.images[idx]))
        orin_img = orin_img.resize((224, 224))
        if self.transform:
            orin_img = np.array(orin_img)
            img = self.transform(image=orin_img)["image"]

        return img, self.labels[idx]


def test(dataloader, model, loss_fn):
    font = ImageFont.truetype("../OpenSans-Regular.ttf", 5)

    model.to(device)
    model.eval()

    loss = 0
    acc = 0

    y = []
    y_hat = []

    with torch.no_grad():
        for images, labels in tqdm(dataloader):

            y.extend(labels.numpy())

            images = images.to(device)
            labels = labels.to(device)

            preds = model(images)

            _preds = preds.argmax(dim=1)
            _loss = loss_fn(preds, labels)

            y_hat.extend(_preds.cpu().detach().numpy())

            acc += (_preds == labels).sum().cpu().detach().numpy().tolist()

            loss += _loss.item()

    loss /= len(dataloader)
    acc = acc / len(dataloader.dataset)

    return loss, acc, y, y_hat


def validate(dataloader, model, loss_fn):
    model.to(device)
    model.eval()

    loss = 0
    acc = 0

    with torch.no_grad():
        for images, labels in tqdm(dataloader):
            images = images.to(device)
            labels = labels.to(device)

            preds = model(images)

            _preds = preds.argmax(dim=1)
            acc += (_preds == labels).sum().cpu().detach().numpy().tolist()

            _loss = loss_fn(preds, labels)
            loss += _loss.item()

    loss /= len(dataloader)
    acc = acc / len(dataloader.dataset)

    return loss, acc


def plot_training_graph(train, val, title="Training loss", y_label="Loss", x_label="Epoch", save_dir=Path("checkpoints")):
    plt.title(title)
    plt.plot(train, color="blue")
    plt.plot(val, color="red")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(str(save_dir / f"{title.replace(' ', '_')}.jpg"))
    plt.clf()


def train(train_loader, val_loader, model, loss_fn, optimizer, epochs, scheduler=None, reduce_on_plateu=None, ckp_dir="checkpoints"):
    ckp_dir = Path(ckp_dir)
    ckp_dir.mkdir(parents=True, exist_ok=True)

    model.to(device)

    training_losses = []
    val_losses = []
    training_accuracies = []
    val_accuracies = []

    pbar = tqdm(range(epochs))

    training_loss = float("nan")
    val_loss = float("nan")
    training_acc = 0.0
    val_acc = 0.0

    best_val_loss = float("inf")
    best_val_acc = 0.0

    for epoch in pbar:

        model.train()

        running_loss = 0
        running_acc = 0
        for images, labels in tqdm(train_loader):
            images = images.to(device)
            labels = labels.to(device)
            optimizer.zero_grad()

            preds = model(images)
            _loss = loss_fn(preds, labels)
            _loss.backward()
            optimizer.step()

            _preds = preds.argmax(dim=1)
            running_acc += (_preds ==
                            labels).sum().cpu().detach().numpy().tolist()

            running_loss += _loss.item()

            pbar.set_description(
                f"Val_loss: {val_loss:.3f}, val_acc: {val_acc:.3f}, loss: {training_loss:.3f}, item_loss: {_loss.item():.3f}, acc: {training_acc:.3f}")

        training_loss = running_loss / len(train_loader)
        training_acc = running_acc / len(train_loader.dataset)

        if reduce_on_plateu:
            reduce_on_plateu.step(val_loss)
        if scheduler:
            scheduler.step()

        val_loss, val_acc = validate(val_loader, model, loss_fn)
        if val_loss < best_val_loss:
            torch.save(model.state_dict(), str(ckp_dir / "best_val_loss.pt"))

        if val_acc > best_val_acc:
            torch.save(model.state_dict(), str(ckp_dir / "best_val_acc.pt"))

        torch.save(model.state_dict(), str(ckp_dir / "last.pt"))

        training_losses.append(training_loss)
        val_losses.append(val_loss)
        training_accuracies.append(training_acc)
        val_accuracies.append(val_acc)

        plot_training_graph(
            training_losses,
            val_losses,
            save_dir=ckp_dir,
        )
        plot_training_graph(
            training_accuracies,
            val_accuracies,
            title="Training_accuracies",
            y_label="Accuracy",
            save_dir=ckp_dir,
        )

    return training_losses, val_losses, training_accuracies, val_accuracies


def main():
    weight = compute_weights_for_wloss(
        Path("../cropped_data/labels.txt"))
    weight = torch.tensor(weight, dtype=torch.float)
    weight = weight.numpy()
    weight = np.clip(weight, 0, 50)
    print(weight.tolist())
    return

    torch.manual_seed(0)
    np.random.seed(0)

    batch_size = 192
    num_workers = 12
    lr = 0.001
    lr_decay = 0.99
    epochs = 50
    n_classes = 107
    weight_decay = 5e-4
    ckp_dir = Path("runs/resnet18_50ep_CE_SAM_107cls_aug3_dataloader2")
    mode = "train"

    # Set this to None if trained from scatch
    # resume_from = Path(
    #     "runs/efficientnetb7_50ep_107cls_aug/best_val_loss.pt")
    resume_from = None

    train_transform = A.Compose([
        A.ShiftScaleRotate(shift_limit=0.05, scale_limit=0.05,
                           rotate_limit=45, p=0.5),
        A.RandomCrop(width=224, height=224),
        A.HorizontalFlip(p=0.5),
        A.VerticalFlip(p=0.5),
        A.AdvancedBlur(p=0.5),
        A.RandomBrightnessContrast(
            brightness_limit=1, contrast_limit=1, p=0.5),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        ToTensorV2(),
    ])

    """
    # Aug1
        T.ToTensor(),
        T.Resize((224, 224)),
        T.RandomVerticalFlip(0.5),
        T.RandomHorizontalFlip(0.5),
        T.RandomRotation(30),
        T.GaussianBlur(3),
        T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    # Aug2
        T.ToTensor(),
        T.Resize((224, 224)),
        T.RandomVerticalFlip(0.5),
        T.RandomHorizontalFlip(0.5),
        T.RandomRotation(180),
        T.GaussianBlur(5),
        T.RandomAutocontrast(0.5),
        T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    """

    transform = A.Compose(
        [
            A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
            ToTensorV2(),
        ]
    )

    train_set = PillDataset(
        root="../cropped_data",
        label_file="train_labels.txt",
        transform=train_transform,
    )
    val_set = PillDataset(
        root="../cropped_data",
        label_file="val_labels.txt",
        transform=train_transform,
        mode="val",
    )
    test_set = PillDataset(
        root="../cropped_data",
        label_file="test_labels.txt",
        transform=transform,
        mode="val",
    )

    # img = next(iter(train_set))[0]
    # img.save("origin.jpg")
    # transform(img).save("augmented.jpg")

    # n_train = int(len(dataset) * train_size)
    # n_val = int(len(dataset) * val_size)
    # n_test = len(dataset) - n_train - n_val
    # train_set, val_set, test_set = random_split(
    #     dataset, [n_train, n_val, n_test])

    train_loader = DataLoader(
        train_set, batch_size=batch_size, num_workers=num_workers, shuffle=True)
    val_loader = DataLoader(
        val_set, batch_size=batch_size, num_workers=num_workers,  shuffle=False)
    test_loader = DataLoader(
        test_set, batch_size=batch_size, num_workers=num_workers,  shuffle=True)

    model = models.resnet18(weights=models.ResNet18_Weights.DEFAULT)
    model.fc = nn.Linear(model.fc.in_features, n_classes)
    print(model)

    if resume_from:
        model.load_state_dict(torch.load(resume_from))

    # Weighted loss
    weight = None
    # weight = compute_weights_for_wloss(
    #     Path("../cropped_data/train_labels.txt"))
    # weight = torch.tensor(weight, dtype=torch.float).to(device)

    loss_fn = nn.CrossEntropyLoss(weight=weight)
    # loss_fn = FocalLoss(gamma=4)
    optimizer = optim.Adam(model.parameters(), lr=lr,
                           weight_decay=weight_decay)

    scheduler1 = optim.lr_scheduler.ReduceLROnPlateau(
        optimizer=optimizer,
        mode='min',
        factor=0.1,
        patience=5,
        threshold=1e-4,
        min_lr=0.00001,
    )
    scheduler2 = optim.lr_scheduler.StepLR(
        optimizer=optimizer,
        step_size=1,
        gamma=lr_decay,
    )

    if mode == "train":
        training_losses, val_losses, training_accuracies, val_accuracies = train(
            train_loader,
            val_loader,
            model,
            loss_fn,
            optimizer,
            epochs,
            scheduler=scheduler2,
            reduce_on_plateu=scheduler1,
            ckp_dir=str(ckp_dir),
        )

    else:
        model.load_state_dict(torch.load(ckp_dir / "best_val_loss.pt"))

        loss, acc, y, y_hat = test(test_loader, model, loss_fn)

        print(loss, acc)

        # t = set(range(108))
        # b = set(y)
        # print(list(filter(lambda x: x not in b, t)))

        # print(classification_report(y, y_hat))

        # cm = confusion_matrix(y, y_hat)

        # acc_per_class = cm.diagonal() / cm.sum(axis=0)
        # print(acc_per_class)

        # disp = ConfusionMatrixDisplay(confusion_matrix=cm,
        #                               display_labels=None)

        # figsize = (11, 11)
        # fig, ax = plt.subplots(figsize=figsize)

        # disp = disp.plot(include_values=False,
        #                  cmap="Blues", ax=ax)

        # plt.title("Confusion matrix")
        # plt.savefig("Confusion_maxtrix.jpg")


if __name__ == "__main__":
    main()
