FROM 20011911/notsecond_vaipe_challenge:latest

WORKDIR /pill
COPY . /pill 
RUN mkdir weights

RUN gdown -O weights/pill_detection_checkpoint.pt --id 1WoG9T5V3hahAHcUlzWR87tLs2AxqJ55r \ 
    && gdown -O weights/pill_recognition_checkpoint.pt --id 1HXwuOME1neP0vyNj-Wt1BBZm_uOQ2JYA \ 
    && gdown -O weights/pill_recognize_resne18.pt --id 14o8uPfjjkvEjTkBq0o9VJeFqJ2YYTGUN \
    && gdown -O weights/pres_detection_checkpoint.pt --id 1G9TXlZxCIxGIvg8k9eP8yH9J_sSGSMkb \
    && gdown -O weights/ViT-L-14-336px.pt --id 1yxt9fOCzP83uQ_NHj8-CJTXO2X_RAQQ0 

RUN mkdir records

CMD ["bash", "run.sh"]
