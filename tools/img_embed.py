'''
Copyright by Hoai Nam Nguyen
Last update at 18/09/2022
'''

import sys

ROOT_PATH = '/pill'
sys.path.append(ROOT_PATH)

import clip

import torch
from torch import nn
from torchvision import transforms

import os
import json
from tqdm import tqdm
from PIL import Image

from cfg.setting import *
from common import Logger


class CLIP_Encoder(nn.Module):
    def __init__(self, clip_vit_l14_336):
        super().__init__()
        self.backbone_clip = clip_vit_l14_336
        self.avgpool1d = nn.AdaptiveAvgPool1d(64)
        
    def forward(self, x):
        x = transforms.functional.resize(x,size=[336, 336])
        x = x/255.0
        x = transforms.functional.normalize(x, 
                                    mean=[0.48145466, 0.4578275, 0.40821073], 
                                    std=[0.26862954, 0.26130258, 0.27577711])
        x = self.backbone_clip.encode_image(x.half())
        x = self.avgpool1d(x)
        return x


def emebedding():
    logger("Start embedding image class with pre-train CLIP models.")
    logger('--------------------------------------------')

    device = f"cuda:{GPU_ID}"
    clip_vit_l14_336, _ = clip.load("/pill/weights/ViT-L-14-336px.pt", device=device)
    model = CLIP_Encoder(clip_vit_l14_336).to(device).eval()

    DICT_OUT = {}
    for image_name in tqdm(os.listdir(IMG_RANDOMCLASS_DIR)):
        filename = os.path.join(IMG_RANDOMCLASS_DIR, image_name)
        img = Image.open(filename).convert('RGB')

        convert_to_tensor = transforms.Compose([transforms.PILToTensor()])
        input_tensor = convert_to_tensor(img)
        input_batch = input_tensor.unsqueeze(0)
        input_batch = input_batch.to(device)

        with torch.no_grad():
            embedding = torch.flatten(model(input_batch)[0]).cpu().data.numpy()
        DICT_OUT[image_name] = embedding.tolist()

    json.dump(DICT_OUT, open(IMG_CLS_EMBED, 'w'), indent=4)

    logger('Done. ')
    logger('--------------------------------------------')

if __name__=="__main__":

    logger = Logger()
    emebedding()
