'''
Copyright by Hoai Nam Nguyen
Last update at 08/09/2022
'''

import sys

ROOT_PATH = '/pill'
sys.path.append(ROOT_PATH)

import os
import time
import warnings

import cv2
import torch
from cfg.setting import *
from tqdm import tqdm

for path in [DETECT_PATH, RECOGNIZE_PATH]:
    sys.path.append(path)

from src.detect.yolor.models.models import Darknet
from src.detect.yolor.utils.datasets import LoadImages
from src.detect.yolor.utils.general import non_max_suppression, scale_coords
from src.detect.yolor.utils.plots import plot_one_box

from common import Logger

warnings.simplefilter("ignore")


def check_box(xmin, ymin, xmax, ymax, im0, conf, eps = 0.05):
    if conf > 0.8:
        return True
        
    h, w, _ = im0.shape
    if (xmin > w*eps) and \
        (ymin > h*eps) and \
            (xmax < w*(1-eps)) and \
                (ymax < h*(1-eps)):
        return True 
    return False


def pill_detect(save_img = False):
    logger('--------------------------------------------')
    logger('Start inference pill detection with YOLO-R model')

    device = 'cuda:0' if GPU_ID else 'cpu'
    half = device != 'cpu'

    model_det = Darknet(CFG_DET_PILL, IMGSIZE_PILL).to(device)
    model_det.load_state_dict(torch.load(WEIGHT_DETECT_PILL, map_location=device)['model'])
    model_det.to(device).eval()

    if half:
        model_det.half()  
    logger("Done load model detect from %s" % WEIGHT_DETECT_PILL)

    dataset = LoadImages(IMAGE_PILL_DIR, img_size=IMGSIZE_PILL, auto_size=64)
    logger("Done load dataset from %s" % IMAGE_PILL_DIR)

    t0 = time.time()

    OUTFILES = open(PILL_DET_OUTFILE, 'w')
    OUTFILES.write("image_name,xmin,ymin,xmax,ymax,conf\n")

    total = 0

    for path, img, im0s, _ in tqdm(dataset):
        image_name = path.split('/')[-1]

        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  
        img /= 255.0  
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        pred = model_det(img, augment=False)[0]
        pred = non_max_suppression(pred, PILL_CONF_THRES, PRES_IOU_THRES, classes=None, agnostic=True)

        for det in pred:  
            im0 = im0s.copy()
            if det is not None and len(det):
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()
                for *xyxy, conf_det, _ in det:
                
                    if not check_box(*xyxy, im0s, conf_det):
                        continue
                    line = (*xyxy,conf_det)
                    line = ('%s') % image_name + (',%g' * len(line)).rstrip() % line + '\n'
                    OUTFILES.write(line)
                    total += 1

                    if save_img:
                        plot_one_box(xyxy, im0, label=None, color=(255,255,255), line_thickness=3)
        
        if save_img:
            if not os.path.exists(SAVE_VISUAL_PILL):
                os.makedirs(SAVE_VISUAL_PILL)
            cv2.imwrite(os.path.join(SAVE_VISUAL_PILL, image_name), im0)

    logger('Total have %g bounding box' % total)
    logger('Done. (%.3fs)' % (time.time() - t0))
    logger('--------------------------------------------')


if __name__ == '__main__':
    logger = Logger()
    with torch.no_grad():
        pill_detect(save_img=VISUALIZE)
