'''
Copyright by Hoai Nam Nguyen
Last update at 18/09/2022
'''

import sys

ROOT_PATH = '/pill'
sys.path.append(ROOT_PATH)

import json
import os
import warnings

import cv2
import pandas as pd
from cfg.setting import *

from PIL import Image
from rapidfuzz.distance import Indel
from tqdm import tqdm

from vietocr.tool.config import Cfg
from vietocr.tool.predictor import Predictor

from common import Logger, plot_one_box

warnings.simplefilter("ignore")


class VietOCR():
    def __init__(self) -> None:
        # self.config = Cfg.load_config_from_name('vgg_seq2seq')
        self.config = Cfg.load_config_from_name('vgg_transformer')

        self.config['cnn']['pretrained'] = False
        self.config['predictor']['beamsearch'] = False
        self.config['device'] = 'cuda:%s' % str(GPU_ID)

        self.model = Predictor(self.config)
    
    def __call__(self, img0, prob = False):
        return self.model.predict(img0, return_prob=prob)


class PreProcess():
    def have_digit(self, text):
        digit = [str(v) for v in range(9)]
        for c in text:
            if c not in digit:
                return False
        return True 

    def remove_digit(self, text):
        texts = text.split()
        result = []
        for text in texts:
            if not self.have_digit(text):
                result.append(text)
        return " ".join(result)

    def __call__(self, text):
        for c in text:
            if c in ".+-,;()/":
                text = text.replace(c, " ")
        text = text.strip()
        text = text.upper()
        text = self.remove_digit(text)
        return text


def pres_recongize(save_img=False):
    logger('--------------------------------------------')
    logger(f'Start inference prescription recognition with ViecOCR on GPU:{GPU_ID}')
    ocr_model = VietOCR()
    pre_process = PreProcess()

    map_presname_classid = json.load(open(MAP_NAME_TO_CLASS_PILL))
    pres_det = pd.read_csv(PRES_CSV_OUTFILE)

    RESULT_MAP = {}
    for image_name in tqdm(sorted(os.listdir(IMAGE_PRES_DIR))):
        img0 = Image.open(os.path.join(IMAGE_PRES_DIR, image_name))
        
        img_det = pres_det[pres_det['image_name'] == image_name]

        if save_img:
            im = cv2.imread(os.path.join(IMAGE_PRES_DIR, image_name))
        
        RESULT_MAP[image_name] = []
        for _, row in img_det.iterrows():
            bbox = list(map(int, [row['xmin'], row['ymin'], row['xmax'], row['ymax']]))
            text = ocr_model(img0.crop(bbox))

            drug_name_pred = pre_process(text)
            max_score = 0
            key_mode = None
            for key in map_presname_classid:
                drug_name_true = pre_process(key)
                curr_score = Indel.normalized_similarity(drug_name_pred, drug_name_true)

                if curr_score > max_score:
                    max_score = curr_score
                    key_mode = key 

            if max_score > 0.5:        
                RESULT_MAP[image_name].extend(map_presname_classid[key_mode])
            else:
                print(image_name)

            if save_img:
                try:
                    label = "%s "% key_mode + "%s " % ' | '.join(map_presname_classid[key_mode])
                    plot_one_box(bbox, im, color=None, label=label)
                except:
                    plot_one_box(bbox, im, color=None, label='')

        if save_img:
            cv2.imwrite(os.path.join(SAVE_VISUAL_PRES, image_name), im)

    json.dump(RESULT_MAP, open(MAP_PRES2PILL_OUTFILE, 'w'), indent=4)

    logger(f'Results save at {MAP_PRES2PILL_OUTFILE}')
    logger('--------------------------------------------')


if __name__=="__main__":
    logger = Logger()
    pres_recongize(save_img=VISUALIZE)
