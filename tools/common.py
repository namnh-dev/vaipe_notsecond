'''
Copyright by Hoai Nam Nguyen
Last update at 08/09/2022
'''

import logging
from logging.handlers import RotatingFileHandler
from logging import Formatter, StreamHandler

import cv2 
from numpy import random


class Logger():
    def __init__(self) -> None:

        self.logger = logging.getLogger('ai4vn-solution')
        self.logger.setLevel(logging.DEBUG)
        self.formatter = Formatter('%(asctime)s - %(levelname)s - %(message)s')
        self.handler = RotatingFileHandler('records/log.log', maxBytes=2000)
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)

        self.streem_handler = StreamHandler()
        self.streem_handler.setLevel(logging.DEBUG)
        self.streem_handler.setFormatter(self.formatter)
        self.logger.addHandler(self.streem_handler)


    def __call__(self, info):
        self.logger.info(info)


def plot_one_box(x, img, color=None, label=None, line_thickness=2):
    tl = line_thickness or round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1  # line/font thickness
    color = color or [random.randint(0, 255) for _ in range(3)]
    c1, c2 = (int(x[0]), int(x[1])), (int(x[2]), int(x[3]))
    cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
    if label:
        tf = max(tl - 1, 1)
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)


if __name__=="__main__":
    logger = Logger()
    logger("-----")
