'''
Copyright by Hoai Nam Nguyen
Last update at 18/09/2022
'''

from asyncio.log import logger
import sys

ROOT_PATH = '/pill'
sys.path.append(ROOT_PATH)

from sklearn.cluster import KMeans

from cfg.setting import *
from common import Logger
import json 


def kmeans_on_pill(n_clusters = 5):
    logger("Pill class cluster with Kmeans")
    logger('--------------------------------------------')

    img_embedd = json.load(open(IMG_CLS_EMBED))
    img_embedd = sorted(img_embedd.items(), key=lambda x: int(x[0][:-4]))
    img_embedd = dict(img_embedd)
    x_feat = list(img_embedd.values())

    kmeans = KMeans(n_clusters=n_clusters, random_state=22)
    kmeans.fit(x_feat)

    labels = kmeans.labels_

    out = {}
    for k, clus in enumerate(labels):
        out[k] = str(clus) 

    json.dump(out, open(PILL_CLUSTER, 'w'), indent=4)

    logger('Done. ')
    logger('--------------------------------------------')

if __name__=="__main__":
    logger = Logger()
    kmeans_on_pill()