'''
Copyright by Hoai Nam Nguyen
Last update at 18/09/2022
'''

import sys
ROOT_PATH = '/pill'
sys.path.append(ROOT_PATH)

import json
import pandas as pd
from tqdm import tqdm

from tools.common import Logger
from cfg.setting import * 
import warnings
warnings.simplefilter("ignore")


class Processer():
    def __init__(self):
        self.pres2pill = json.load(open(MAP_PRES2PILL_OUTFILE))
        self.weight = [10, 8, 5, 5] 
        self.cluster = json.load(open(PILL_CLUSTER))

    def _scale_cluster(self, merge_cls):
        w = []
        for cls in merge_cls:
            cnt = 0
            for cls_ in merge_cls:
                if cls == cls_:
                    continue
                if self.cluster[str(cls)] == self.cluster[str(cls_)]:
                    cnt += 1
            w.append(cnt/len(merge_cls))
        
        return w 

    def compute(self, X):
        score = self.weight[0]*X[0] \
                + self.weight[1]*(X[1]) \
                + self.weight[2]*(X[2]) \
                + self.weight[3]*(1 - X[3]) \

        return score 

    def __call__(self, pres_name, image_name, df_result_fgvc, df_result_resnet):

        key_id = pres_name + '.png'
        isMAPPED = list(map(int, self.pres2pill[key_id]))

        # id_ = image_name.split("_")[2]
        # key_id = "VAIPE_P_TEST_%s.png" % id_ 
        # isMAPPED = list(map(int, self.pres2pill[key_id]))

        df_output = df_result_fgvc.copy() 

        
        for row_fgvc, row_resnet in zip(df_result_fgvc.iterrows(), df_result_resnet.iterrows()):            
            id_class = row_fgvc[0]

            topk_cls_fgvc = list(map(int, row_fgvc[1]['class_id'].split('#')))
            topk_cls_resnet = list(map(int, row_resnet[1]['class_id'].split('#')))            
            merge_cls = topk_cls_fgvc.copy()

            for cls in topk_cls_resnet:
                if not cls in merge_cls:
                    merge_cls.append(cls)

            scale_clus = self._scale_cluster(merge_cls)

            scores_vote = []
            for sc, cls in zip(scale_clus, merge_cls):
                X = [0 for _ in range(4)]
                if cls in isMAPPED:
                    X[0] = 1
                if cls in topk_cls_fgvc:
                    X[1] = (2 - topk_cls_fgvc.index(cls))/2
                if cls in topk_cls_resnet:
                    X[2] = (2 - topk_cls_resnet.index(cls))/2
                X[3] = sc

                score = self.compute(X)
                scores_vote.append(score)

            idx = scores_vote.index(max(scores_vote))
            class_id = merge_cls[idx]

            if class_id not in isMAPPED:
                class_id = 107 
            
            df_output['class_id'][id_class] = class_id

        return df_output

