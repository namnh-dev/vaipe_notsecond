'''
Copyright by Hoai Nam Nguyen
Last update at 18/09/2022
'''
import sys
ROOT_PATH = '/pill'
sys.path.append(ROOT_PATH)

import json
import pandas as pd
from tqdm import tqdm

from tools.common import Logger
from cfg.setting import * 

from tools.process import Processer

import warnings
warnings.simplefilter("ignore")

def main():
    logger('--------------------------------------------')
    logger('Start runing post process and create final output')
    
    process = Processer()

    OUTFILES = open(OUT_FINAL, 'w')
    OUTFILES.write("image_name,class_id,confidence_score,x_min,y_min,x_max,y_max\n")

    RESULTS_FGVC = pd.read_csv(PILL_REG_OUTFILE)
    RESULTS_RESNET = pd.read_csv(PILL_REG_RESNET_OUTFILE)

    PRES_MAP = json.load(open(PRES_MAP_FILE))

    for pres_name in tqdm(list(PRES_MAP.keys())):
        for image_name in PRES_MAP[pres_name]:
            df_result_fgvc = RESULTS_FGVC[RESULTS_FGVC['image_name'] == image_name]
            df_result_resnet = RESULTS_RESNET[RESULTS_RESNET['image_name'] == image_name]

            if not len(df_result_fgvc) and not len(df_result_resnet):
                print(f"image not have result {image_name}")
                continue
            
            df_out = process(pres_name, image_name, df_result_fgvc, df_result_resnet)

            for _, row in df_out.iterrows():
                line = f"{row['image_name']},{row['class_id']},{row['confidence_score']},{row['x_min']},{row['y_min']},{row['x_max']},{row['y_max']}\n"
                OUTFILES.write(line) 

    logger('Done. ')
    logger(f'Results save at {OUT_FINAL}')
    logger('--------------------------------------------')


if __name__=="__main__":
    logger = Logger()
    main()
