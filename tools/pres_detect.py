'''
Copyright by Hoai Nam Nguyen
Last update at 08/09/2022
'''


import sys

ROOT_PATH = '/pill'
sys.path.append(ROOT_PATH)

from cfg.setting import *

sys.path.append(PRES_DETECT_PATH)

import os
import time
import warnings

import cv2
import torch
from numpy import random

from src.detect.yolov7.models.experimental import attempt_load
from src.detect.yolov7.utils.datasets import LoadImages
from src.detect.yolov7.utils.general import (check_img_size,
                                             non_max_suppression, scale_coords)
from src.detect.yolov7.utils.plots import plot_one_box
from src.detect.yolov7.utils.torch_utils import select_device

from tqdm import tqdm

from common import Logger

warnings.simplefilter("ignore")


def detect_pres(save_img=False):

    logger('--------------------------------------------')
    logger(f'Start inference prescription detection with YOLO7 model on GPU:{GPU_ID}')
    device = select_device(str(GPU_ID))
    # Load model
    model = attempt_load(WEIGHT_DETECT_PRES, map_location=device)  # load FP32 model
    logger(f"Done load model from weight: {WEIGHT_DETECT_PRES}")

    stride = int(model.stride.max())  # model stride
    imgsz = check_img_size(IMGSIZE_PRES, s=stride)  # check img_size

    half = device.type != 'cpu'  # half precision only supported on CUDA
    if half:
        model.half()  # to FP16

    dataset = LoadImages(IMAGE_PRES_DIR, img_size=imgsz, stride=stride)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in names]

    # Run inference
    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))  # run once
    
    t0 = time.time()

    OUTFILES = open(PRES_CSV_OUTFILE, 'w')
    OUTFILES.write('image_name,class,xmin,ymin,xmax,ymax,conf\n')

    for path, img, im0s, _ in tqdm(dataset, total=len(dataset)):
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        image_name = path.split('/')[-1]
        im0 = im0s.copy()
        # Inference
        pred = model(img, augment=False)[0]
        # Apply NMS
        pred = non_max_suppression(pred, PRES_CONF_THRES, PRES_IOU_THRES, classes=None, agnostic=True)

        for i, det in enumerate(pred):  # detections per image
            if len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                for *xyxy, conf, cls in reversed(det):
                    line = (cls,*xyxy,conf)
                    line = ('%s') % image_name + (',%g' * len(line)).rstrip() % line + '\n'
                    OUTFILES.write(line)

                    if save_img:  
                        plot_one_box(xyxy, im0, label=None, color=colors[int(cls)], line_thickness=3)

        if save_img:
            if not os.path.exists(SAVE_VISUAL_PRES):
                os.makedirs(SAVE_VISUAL_PRES)
            save_path = os.path.join(SAVE_VISUAL_PRES, image_name)
            cv2.imwrite(save_path, im0)
    logger(f'Done. ({time.time() - t0:.3f}s)')
    logger(f'Results save at {PRES_CSV_OUTFILE}')
    logger('--------------------------------------------')


if __name__ == '__main__':
    logger = Logger()
    with torch.no_grad():
        detect_pres(save_img=VISUALIZE)
