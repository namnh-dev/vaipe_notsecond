'''
Copyright by Hoai Nam Nguyen
Last update at 08/09/2022
'''

import sys

ROOT_PATH = '/pill'

import os

from PIL import Image

sys.path.append(ROOT_PATH)
import cv2
import pandas as pd
import torch
import torch.nn as nn
import torchvision.models as models
import torchvision.transforms as T
from cfg.setting import *
from tqdm import tqdm

from common import Logger


class Vaipe_Resnet():
    def __init__(self, device, weight_path) -> None:

        self.transform = T.Compose([
        T.ToTensor(),
        T.Resize((224, 224)),
        T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        ])

        self.model = models.resnet18(weights=models.ResNet18_Weights.DEFAULT)
        self.model.fc = nn.Linear(self.model.fc.in_features, 107)
        self.model.load_state_dict(torch.load(weight_path))

        self.model.to(device)
        self.model.eval()

        self.device = device 
    
    def __call__(self, img):
        img = self.transform(img)
        img = img.unsqueeze(0).to(self.device)
        with torch.no_grad():
            outputs = self.model(img)
            outputs = nn.Softmax(dim=1)(outputs).topk(3, dim=1)  
         
        return outputs.indices[0].tolist(), max(outputs.values[0].tolist())


def inference():
    device = 'cuda:0' if GPU_ID else 'cpu'
    resnet_recognize = Vaipe_Resnet(device, WEIGHT_RECOGNIZE_PILL_RESNET18)

    logger('--------------------------------------------')
    logger('Start inference pill recognition with resnet')

    res_det = pd.read_csv(PILL_DET_OUTFILE)
    OUTFILE = open(PILL_REG_RESNET_OUTFILE, 'w')
    OUTFILE.write('image_name,class_id,x_min,y_min,x_max,y_max,confidence_score,conf_reg\n')

    for image_name in tqdm(sorted(os.listdir(IMAGE_PILL_DIR))):
        img0 = cv2.imread(os.path.join(IMAGE_PILL_DIR, image_name))
        img0 = cv2.cvtColor(img0, cv2.COLOR_BGR2RGB)

        res_det_img = res_det[res_det['image_name']==image_name]
        if not len(res_det_img):
            continue

        for _, row in res_det_img.iterrows():
            cropped = img0[int(row['ymin']):int(row['ymax']),int(row['xmin']):int(row['xmax'])]
            cropped = Image.fromarray(cropped)
            topk_cls, conf_reg = resnet_recognize(cropped)
            line = f"{image_name},{'#'.join(list(map(str,topk_cls)))},{row['xmin']},{row['ymin']},{row['xmax']},{row['ymax']},{row['conf']},{conf_reg}\n"
            OUTFILE.write(line)

    logger('Done. ')

if __name__ == "__main__":
    logger = Logger()
    inference()
