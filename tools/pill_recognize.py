
'''
Copyright by Hoai Nam Nguyen
Last update at 08/09/2022
'''

import os
import sys
import warnings

import cv2
import pandas as pd
import torch
import torchvision.transforms as transforms
from PIL import Image
from tqdm import tqdm

ROOT_PATH = '/pill'
sys.path.append(ROOT_PATH)
from cfg.setting import *
for path in [RECOGNIZE_PATH]:
    sys.path.append(path)
import yaml
from src.classify.models.builder import MODEL_GETTER

from common import Logger

warnings.simplefilter("ignore")


class Classifier():
    def __init__(self) -> None:
        self.device = 'cuda:0' if GPU_ID != 'cpu' else 'cpu'
        self.data_size = IMGSIZE_CLS
        self.opt = self.load_yaml()

        self.model_reg = MODEL_GETTER[self.opt['model_name']](use_fpn=self.opt['use_fpn'],
                                            fpn_size=self.opt["fpn_size"],
                                            use_selection=self.opt['use_selection'],
                                            num_classes=self.opt['num_classes'],
                                            num_selects=self.opt['num_selects'],
                                            use_combiner=self.opt['use_combiner'],
                                            )  
        self.model_reg.load_state_dict(torch.load(WEIGHT_RECOGNIZE_PILL, map_location=self.device)['model'])
        self.model_reg.to(self.device).eval()       

    def load_yaml(self):
        with open(CFG_CLS_PILL, 'r', encoding='utf-8') as fyml:
            opt = yaml.load(fyml.read(), Loader=yaml.Loader)      
        return opt

    def transform_tensor(self, img):
        img_transforms = transforms.Compose([
                transforms.Resize((510, 510), Image.BILINEAR),
                transforms.CenterCrop((self.data_size, self.data_size)),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])
        ])

        img = Image.fromarray(img)
        img = img_transforms(img)
        return img.to(self.device)

    def __call__(self, img0, topk=3):
        img = img0[:, :, ::-1]
        img = self.transform_tensor(img)

        out = self.model_reg(img.unsqueeze(0))
        logits = out['comb_outs']
        tmp_score = torch.softmax(logits, dim=-1)

        ids = torch.topk(tmp_score[0],topk).indices
        topk = [int(x) for x in ids.tolist()]

        return topk, float(torch.max(tmp_score))


def pill_classify():
    logger('--------------------------------------------')
    logger('Start inference pill recognition')

    res_det = pd.read_csv(PILL_DET_OUTFILE)
    OUTFILE = open(PILL_REG_OUTFILE, 'w')
    OUTFILE.write('image_name,class_id,x_min,y_min,x_max,y_max,confidence_score,conf_reg\n')

    classifier = Classifier()

    for image_name in tqdm(sorted(os.listdir(IMAGE_PILL_DIR))):
        img0 = cv2.imread(os.path.join(IMAGE_PILL_DIR, image_name))

        res_det_img = res_det[res_det['image_name']==image_name]
        if not len(res_det_img):
            continue

        for _, row in res_det_img.iterrows():
            cropped = img0[int(row['ymin']):int(row['ymax']),int(row['xmin']):int(row['xmax'])]
            topk_cls, conf_reg = classifier(cropped)

            line = f"{image_name},{'#'.join(list(map(str,topk_cls)))},{row['xmin']},{row['ymin']},{row['xmax']},{row['ymax']},{row['conf']},{conf_reg}\n"
            OUTFILE.write(line)

    logger('Done. ')
            
if __name__=="__main__":
    logger = Logger()
    pill_classify()



